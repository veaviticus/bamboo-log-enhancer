function isJson(str) {
    data = {}
    try {
        data = JSON.parse(str);
    } catch (e) {
        return {valid: false};
    }
    return {valid: true, data: data};
}


(function() {
    var logLineTypes = [
        'build',
        'command',
        'error',
        'simple'
    ];

    var colorCodes = {
        '30': 'black',
        '31': 'red',
        '32': 'green',
        '33': 'yellow',
        '34': 'blue',
        '35': 'magenta',
        '36': 'cyan',
        '37': 'black'
    };
    var body = document.body
    var logLines = body.innerText.split('\n');

    firstLine = logLines[0];
    firstWord = firstLine.split("\t").shift();
    if (logLineTypes.indexOf(firstWord) == -1 ){
        return;
    }

    var logDiv = document.createElement('div');
    logDiv.className = "log";

    var currentLogBlock;

    body.children[0].remove();

    for (var i = 0; i < logLines.length; ++i) {
        lineElement = document.createElement('div');

        logLine = logLines[i];
        blocks = logLine.split('\t');
        lineType = blocks.shift();
        time = blocks.shift();

        firstWordElement = document.createElement('span');

        if (logLineTypes.indexOf(lineType) >= 0){
            logLineType = lineType;
        } else {
            logLineType = 'default';
        }

        firstWordElement.className = logLineType;
        firstWordElement.innerText = logLineType + "\t";
        lineElement.appendChild(firstWordElement);

        textElement = document.createElement('span');
        textElement.className = 'default';
        textElement.innerText = time + '\t';
        lineElement.appendChild(textElement);

        restOfLine = blocks.join('\t');

        splitLines = restOfLine.split('\\n');
        for (var splitIndex = 0; splitIndex < splitLines.length; ++splitIndex) {
            if (splitIndex > 1) {
                elType = 'div';
                elClass = 'indent';
            }
            else {
                elType = 'span';
                elClass = 'default';
            }
            restOfLineElement = document.createElement(elType);
            restOfLineElement.className = elClass;
            parseLine = splitLines[splitIndex];
            while(parseLine.length > 0) {
                colorIndex = parseLine.indexOf('\033[1;');
                colorOffset = 4;
                if (colorIndex == -1) {
                    colorIndex = parseLine.search(/\[[0-9]{2}m/);
                    colorOffset = 1;
                }
                if (colorIndex > -1) {
                    before = parseLine.substring(0, colorIndex);
                    beforeColor = document.createElement('span');
                    beforeColor.className = 'default';
                    beforeColor.innerText = before;
                    lineElement.appendChild(beforeColor);
                    colorCode = parseLine.substring(colorIndex+colorOffset, colorIndex+colorOffset+2);
                    colorElement = document.createElement('span');
                    colorElement.className = colorCodes[colorCode];
                    after = parseLine.substring(colorIndex+colorOffset+3, parseLine.length);
                    resetIndex = after.indexOf('\033[0m');
                    resetOffset = 4;
                    if (resetIndex == -1) {
                        resetIndex = parseLine.indexOf('[m');
                        resetOffset = 2;
                    }
                    if (resetIndex > -1) {
                        colorText = after.substring(0, resetIndex);
                        colorElement.innerText = colorText;
                        lineElement.appendChild(colorElement);
                        parseLine = after.substring(resetIndex+resetOffset, after.length);
                    }
                    else  {
                        colorElement.innerText = after;
                        lineElement.appendChild(colorElement);
                        parseLine = "";
                    }
                }
                else {
                    maybeJsonIndex = parseLine.search(/\{.*\}/);
                    if (maybeJsonIndex > -1) {
                        last = parseLine.lastIndexOf('}')+1;
                        jsonStr = parseLine.substring(
                            maybeJsonIndex,
                            last
                        );
                        maybeJson = isJson(jsonStr);
                        if (maybeJson.valid) {
                            innerTextStr = parseLine.substring(0, maybeJsonIndex);
                            innerTextStr += "\n";
                            innerTextStr += JSON.stringify(maybeJson.data, null, 4);
                            innerTextStr += parseLine.substring(last, parseLine.length);
                            restOfLineElement.innerText = innerTextStr;
                        }
                        else {
                            restOfLineElement.innerText = parseLine;
                        }
                    }
                    else {
                        restOfLineElement.innerText = parseLine;
                    }
                    parseLine = "";
                }
            }
            lineElement.appendChild(restOfLineElement);
        }
        logDiv.appendChild(lineElement);
    }
    body.appendChild(logDiv);
})();