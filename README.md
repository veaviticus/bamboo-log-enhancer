# Bamboo Log Enhancer

Simple WebExtension that will add color to your Atlassian Bamboo log files

Compatible with Chrome and Firefox Quantum

## Installation

1. Clone this repo: `git clone https://gitlab.com/veaviticus/bamboo-log-enhancer.git`;

### Chrome
1. Open Chrome and go to your extensions: `chrome://extensions`;
2. Make sure *Developer mode* is enabled;
3. Click *Load unpacked extension...* and select `bamboo-log-enhancer/extension`.

### Firefox
1. Open Firefox and go to the debugging page: `about:debugging`
2. Click Load Temporary Add-on
3. Browse and select the `bamboo-log-enhancer/extension/manifest.json` file